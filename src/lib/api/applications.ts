import axios from "axios";
import * as config from "$lib/config.json";
import type * as common from "./common";

export enum ApplicationState {
    Uninstalled = "uninstalled",
    Downloaded = "downloaded",
    Installed = "installed",
};

export interface TargetState {
    target_state: ApplicationState,
    target_version: string,
}

export interface Stated extends TargetState {
    current_state: ApplicationState,
    current_version: string,
}

export interface ApplicationDisplaySettings {
    visible: boolean,
    weight: number,
}

export interface Application extends ApplicationDisplaySettings, Stated, common.Linked {
    bundle: string,

    name: string,

    repository_version: string,
    has_content_to_upgrade: boolean,
}

export interface Content extends Stated, common.Linked {
    content_id: string,
    
    name: string,
    subject: string,
    description: string,

    size: number,
    language: string,
    repository_version: string,
}

export interface Sysinfo {
    free_disk: string,
    total_disk: string,
}

export enum EndpointType {
    Application = "application",
    Content = "content",
}

export interface Endpoint {
    name: string,
    url: string,
    type: EndpointType
}

export async function listApplications(options: {
    /**
     * Specify whether to list uninstalled, downloaded, or installed applications
     */
    current_state: ApplicationState,
    /**
     * Specify whether you'd like (in)visible applications
     */
    visible: boolean,
    /**
     * Specify whether the applications repository should be updated before a response is returned
     * This option is rather unusual, which is why it's optional
     */
    repository_update?: boolean,
} = {
    current_state: null,
    visible: null,
    repository_update: false,
}) : Promise<Application[]> {
    let url = new URL(config.api_url + "/applications/");

    if (options.current_state)
        url.searchParams.append("current_state", options.current_state);

    if (options.visible)
        url.searchParams.append("visible", options.visible.toString());

    if (options.repository_update)
        url.searchParams.append("repository_update", options.repository_update.toString());

    let req = await axios.get(url.toString());

    return req.data.data;
}

export async function getApplication(appUrl: string): Promise<Application> {
    let req = await axios.get(appUrl);

    return req.data;
}

export async function getApplicationTargetState(appTargetStateUrl: string): Promise<TargetState> {
    let req = await axios.get(appTargetStateUrl);

    return req.data;
}

export async function setApplicationTargetState(appTargetStateUrl: string, state: TargetState): Promise<void> {
    await axios.put(appTargetStateUrl, state);
}

export async function setApplicationDisplaySettings(appDisplaySettingUrl: string, display_settings: ApplicationDisplaySettings): Promise<void> {
    await axios.put(appDisplaySettingUrl, display_settings);
}

export async function getApplicationContent(appContentUrl: string): Promise<Content> {
    let content = await axios.get(appContentUrl);

    return content.data.data;
}

export async function setApplicationContentTargetState(contentTargetState: string, state: TargetState): Promise<void> {
    try {
        return await axios.put(contentTargetState, state);
    } catch (e) {
        return e.response;
    }
}

export async function getApplicationConfiguration(configurationUrl: string) {
    let req = await axios.get(configurationUrl);

    return req.data;
}

export async function putApplicationConfiguration(configurationUrl: string, configuration) {
    let req = await axios.put(configurationUrl, configuration);

    return req.data;
}

export async function getSysinfo(): Promise<Sysinfo> {
    let req = await axios.get(config.api_url + "/applications/sysinfo");

    return req.data;
}

export async function listApplicationEndpoints(endpointsUrl: string, options: {
    withContainers: boolean,
    withContents: boolean,
    categoryId: string,
    playlistId: string,   
} = {
    withContainers: true,
    withContents: false,
    categoryId: null,
    playlistId: null,
}): Promise<Endpoint[]> {
    let url = new URL(endpointsUrl);

    let withContainers = options.withContainers;
    let withContents = options.withContents;
    let categoryId = options.categoryId;
    let playlistId = options.playlistId;

    if (withContainers)
        url.searchParams.append("with_containers", withContainers.toString());

    if (withContents)
        url.searchParams.append("with_contents", withContents.toString());

    if (categoryId)
        url.searchParams.append("category_id", categoryId);

    if (playlistId)
        url.searchParams.append("playlist_id", playlistId);

    let req = await axios.get(url.toString());

    return req.data.data;
}
