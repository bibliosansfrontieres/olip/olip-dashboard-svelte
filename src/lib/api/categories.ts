import axios from "axios";
import * as config from "$lib/config.json";
import type * as common from "./common";
import type { Content } from "./applications";

export interface CategoryListing extends common.Linked, common.Labeled, common.Tagged {
    id: number,
}

export interface Category extends common.Linked, common.Labeled, common.Tagged {
    id: number,
    applications: string[],
    contents: Content[],
    playlists: number[],
}

export async function listCategories(): Promise<CategoryListing[]> {
    let req = await axios.get(config.api_url + "/categories/");
    return req.data.data;
}

export async function createCategory(data: Category): Promise<Category> {
    let req = await axios.post(config.api_url + "/categories/", data);
    return req.data;
}

export async function updateCategory(categoryId: string | number, data: Category): Promise<Category> {
    const copiedData = {...data};
    delete copiedData.id;

    if (!copiedData.contents) copiedData.contents = [];
    if (!copiedData.directLinks) copiedData.directLinks = [];

    let req = await axios.put(`${config.api_url}/categories/${categoryId}`, copiedData);

    return req.data;
}

export async function updateCategoryThumbnail(categoryThumbnailUrl, contentType, contentLength, content, progress) {
    await axios.put(categoryThumbnailUrl, content, {
        onUploadProgress (progressEvent) {
            progress(progressEvent.loaded)
        },
        headers: {
            "Content-Type": contentType,
            "Content-Length": contentLength
        }
    });
}

export async function getCategory(categoryId: string | number): Promise<Category> {
    let req = await axios.get(`${config.api_url}/categories/${categoryId}`);

    return req.data;
}

export async function deleteCategory(categoryId: string | number) {
    await axios.delete(config.api_url + "/categories/" + categoryId);
}

export async function getCategoryLinkIconUrl(categoryId, linkIndex) {
    return `${config.api_url}/categories/${categoryId}/link-icon/${linkIndex}`;
}

export async function deleteCategoryLinkIcon(categoryId, linkIndex) {
    let url = await this.getCategoryLinkIconUrl(categoryId, linkIndex);
    await axios.delete(url);
}

export async function setCategoryDisplaySettings(categoryId: string | number, weight: number) {
    await axios.put(config.api_url + "/categories/display-settings", {
        id: categoryId,
        weight: weight
    });
}
