import axios from "axios";
import * as config from "$lib/config.json";
import type * as common from "./common";

export interface Term extends common.Linked {
    term: string,
    custom: boolean,
}    

export async function listTerms(): Promise<Term> {
    let req = await axios.get(config.api_url + "/terms/");

    return req.data.data;
}

export async function createTerm(term: Term): Promise<Term> {
    let req = await axios.post(config.api_url + "/terms/", { term: term });

    return req.data;
}

export async function deleteTerm(termUrl: string): Promise<void> {
    await axios.delete(termUrl);
}
