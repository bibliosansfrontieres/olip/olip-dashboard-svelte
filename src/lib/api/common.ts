export enum LinkRelationship {
    Self = "self",
    Picture = "picture",
    TargetState = "target-state",
    Endpoints = "endpoints",
    DisplaySettings = "display-settings",
    Contents = "contents",
};

export interface Link {
    href: string,
    rel: LinkRelationship,
};

export interface Linked {
    links: Link[],
}

export interface Labels {
    ab?: string,
    aa?: string,
    af?: string,
    sq?: string,
    am?: string,
    ar?: string,
    hy?: string,
    as?: string,
    ay?: string,
    az?: string,
    ba?: string,
    eu?: string,
    bn?: string,
    dz?: string,
    bh?: string,
    bi?: string,
    br?: string,
    bg?: string,
    my?: string,
    be?: string,
    km?: string,
    ca?: string,
    zh?: string,
    co?: string,
    hr?: string,
    cs?: string,
    da?: string,
    nl?: string,
    en?: string,
    eo?: string,
    et?: string,
    fo?: string,
    fj?: string,
    fi?: string,
    fr?: string,
    fy?: string,
    gd?: string,
    gl?: string,
    ka?: string,
    de?: string,
    el?: string,
    kl?: string,
    gn?: string,
    gu?: string,
    ha?: string,
    iw?: string,
    hi?: string,
    hu?: string,
    is?: string,
    in?: string,
    ia?: string,
    ie?: string,
    ik?: string,
    ga?: string,
    it?: string,
    ja?: string,
    jw?: string,
    kn?: string,
    ks?: string,
    kk?: string,
    rw?: string,
    ky?: string,
    rn?: string,
    ko?: string,
    ku?: string,
    lo?: string,
    la?: string,
    lv?: string,
    ln?: string,
    lt?: string,
    mk?: string,
    mg?: string,
    ms?: string,
    ml?: string,
    mt?: string,
    mi?: string,
    mr?: string,
    mo?: string,
    mn?: string,
    na?: string,
    ne?: string,
    no?: string,
    oc?: string,
    or?: string,
    om?: string,
    ps?: string,
    fa?: string,
    pl?: string,
    pt?: string,
    pa?: string,
    qu?: string,
    rm?: string,
    ro?: string,
    ru?: string,
    sm?: string,
    sg?: string,
    sa?: string,
    sr?: string,
    sh?: string,
    st?: string,
    tn?: string,
    sn?: string,
    sd?: string,
    si?: string,
    ss?: string,
    sk?: string,
    sl?: string,
    so?: string,
    es?: string,
    su?: string,
    sw?: string,
    sv?: string,
    tl?: string,
    tg?: string,
    ta?: string,
    tt?: string,
    te?: string,
    th?: string,
    bo?: string,
    ti?: string,
    to?: string,
    ts?: string,
    tr?: string,
    tk?: string,
    tw?: string,
    uk?: string,
    ur?: string,
    uz?: string,
    vi?: string,
    vo?: string,
    cy?: string,
    wo?: string,
    xh?: string,
    ji?: string,
    yo?: string,
    zu?: string,
}

export interface Labeled {
    labels: Labels,
}

export interface Tagged {
    tags: string[],
}
