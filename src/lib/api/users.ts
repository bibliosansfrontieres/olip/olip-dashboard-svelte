import axios from "axios";
import * as config from "$lib/config.json";
import type * as common from "./common";

export interface UserStub {
    name: string,
    url: string,
    provider?: string,
}

export interface User extends common.Linked {
    username: string

    provider?: string,
    name: string,
    admin: boolean,
}

export async function listUsers(): Promise<User[]> {
    let req = await axios.get(config.api_url + "/users/");

    return req.data.data;
}

export async function getUser(username: string): Promise<User> {
    let req = await axios.get(`${config.api_url}/users/${username}`);

    return req.data.data;
}

export async function saveUser(username: string, user: User): Promise<User> {
    let req = await axios.put(`${config.api_url}/users/${username}`, user);

    return req.data.data;
}

export async function deleteUser(username: string): Promise<void> {
    await axios.delete(config.api_url + "/users/" + username);
}
