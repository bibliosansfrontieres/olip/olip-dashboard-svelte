import axios from "axios";
import * as config from "$lib/config.json";
import type * as common from "./common";
import type { UserStub } from "./users";

export interface PlaylistListing extends common.Linked {
    id: number,
    title: string,
    user: UserStub
}

export interface PlaylistContent {
    bundle: string,
    content_id: string,
}

export interface DirectLink {
    id: 0,
    href: string,
    title: string,
    
    playlist_id: number,
    index: number,

    icon: string,
    icon_mimetype: string,
}

export interface Playlist extends common.Linked {
    id: number,
    title: string,
    user: UserStub,
    pinned: boolean,

    applications: string[],
    contents: PlaylistContent[],
    direct_links: DirectLink[],
}

export async function getPlaylist(playlistUrl: string) {
    let req = await axios.get(playlistUrl);

    return req.data;
}

export async function listPlaylists(categoryId?: string | number, visibleForUser?: boolean): Promise<PlaylistListing[]> {
    let url = new URL(config.api_url + "/playlists/");

    if (categoryId)
        url.searchParams.append("category_id", categoryId.toString());

    if (visibleForUser)
        url.searchParams.append("visible_for_user", visibleForUser.toString());

    let req = await axios.get(url.toString());

    return req.data.data;
}

export async function updatePlaylist(playlistUrl: string, data: Playlist): Promise<Playlist> {
    let req = await axios.put(playlistUrl, data);

    return req.data;
}

export async function createPlaylist(data: Playlist): Promise<Playlist> {
    let req = await axios.post(config.api_url + "/playlists/", data);

    return req.data;
}

export async function deletePlaylist(playlistId: string | number): Promise<void> {
    await axios.delete(config.api_url + "/playlists/" + playlistId);
}

export function getPlaylistLinkIconUrl(playlistId: string | number, linkIndex: number): string {
    return `${config.api_url}/playlists/${playlistId}/link-icon/${linkIndex}`;
}

export async function deletePlaylistLinkIcon(playlistId: string | number, linkIndex: number) {
    let url = await this.getPlaylistLinkIconUrl(playlistId, linkIndex);
    await axios.delete(url);
}

export async function setPlaylistDisplaySettings(playlistId: string | number, weight: number) {
    await axios.put(config.api_url + "/playlists/display-settings", {
        id: playlistId,
        weight: weight
    });
}
