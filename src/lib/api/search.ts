import axios from "axios";
import * as config from "$lib/config.json";

export async function search(searched: string) {
    let req = await axios.get(`${config.api_url}/opensearch/search?q=${searched}`, {
        responseType: "document"
    });

    return req.data;
}
