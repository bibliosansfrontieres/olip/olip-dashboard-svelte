# olip-dashboard-svelte

This project contains a frontend for OLIP written in Svelte.

## Installing

```bash
# clone repo
git clone https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard-svelte.git

# install deps
cd olip-dashboard-svelte
npm i --include=dev

# do the hacks
npm run vite-workaround
npm run smui-theme-light
```

## Developing

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
